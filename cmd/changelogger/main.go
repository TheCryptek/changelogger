package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/bkc/changelogger/clog"
)

const (
	prUrlFormat  = "https://github.com/go-gitea/gitea/pull/%d"
	tagUrlFormat = "https://github.com/go-gitea/gitea/releases/tags/v%s"
)

func main() {
	var (
		changelogDir = flag.String("changelogDir", "changelog", "Directory to look for changelogs")
		version      = flag.String("version", "", "Version for changelog")
		outFile      = flag.String("outputFile", "CHANGELOG2.md", "File to which the changelog is written")
		force        = flag.Bool("force", false, "Force update of changelog even if version already exists. DON'T USE THIS!")
	)
	flag.Parse()

	var (
		unrelDir  = *changelogDir + "/unreleased"
		relDir    = *changelogDir + "/v"
		relDirFmt = relDir + "%s"
	)

	if version == nil || *version == "" {
		fmt.Printf("Error: You did not give a version\n\nAvailable flags:\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	verDir := fmt.Sprintf(relDirFmt, *version)

	if _, err := os.Stat(verDir); err == nil {
		if !(*force) {
			log.Println("Error: Version already released! Aborting!")
			os.Exit(1)
		}
		log.Println("Warning: Version already released! Continuing anyhow!")
	}

	log.Printf("Creating new changelog directory: %q\n", verDir)
	if err := os.Mkdir(verDir, 0755); err != nil && !(*force) {
		log.Printf("Error: creating changelog directory failed: %q: %v\n", verDir, err)
		os.Exit(1)
	}

	log.Printf("Copying unreleased changelogs\n")
	if err := filepath.Walk(unrelDir, copyFileWalker(unrelDir, verDir)); err != nil {
		log.Printf("Error: copying files: %v\n", err)
		os.Exit(1)
	}

	log.Printf("Generating new changelog\n")
	cl := clog.New(*changelogDir, relDir, unrelDir)
	if err := filepath.Walk(*changelogDir, cl.Populate(unrelDir)); err != nil {
		fmt.Printf("Error: generating changelog: %v\n", err)
		os.Exit(1)
	}

	outputFile, err := os.Create(*outFile)
	if err != nil {
		log.Printf("Error: opening changelog failed: %q: %v", *outFile, err)
	}
	defer outputFile.Close()

	if err := cl.Write(outputFile); err != nil {
		fmt.Printf("Error: writing changelog: %v\n", err)
		os.Exit(1)
	}

	log.Printf("Removing released changelogs\n")
	if err := filepath.Walk(unrelDir, removeFileWalker(unrelDir)); err != nil {
		log.Printf("Error: removing files: %v\n", err)
		os.Exit(1)
	}
}

func removeFileWalker(dir string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if err := os.Remove(path); err != nil {
			return fmt.Errorf("failed to remove file: %q: %s", path, err)
		}
		log.Printf(" => Changelog removed: %q\n", path)
		return nil
	}
}

func copyFileWalker(oldDir, newDir string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}

		// NOTE: Since the generator loads the entire file into memory.
		//       Don't include files that are larger than 1kB
		if info.Size() >= 1024 {
			log.Printf("Warning: file to large, skipping: %q %d\n", path, info.Size())
			return nil
		}

		newPath := strings.Replace(path, oldDir, newDir, 1)
		oldFile, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("failed to open file: %q", path)
		}
		defer oldFile.Close()

		newFile, err := os.Create(newPath)
		if err != nil {
			return fmt.Errorf("failed to open file: %q", newPath)
		}
		defer newFile.Close()

		if _, err := io.Copy(newFile, oldFile); err != nil {
			return fmt.Errorf("failed to copy file: %q => %q: %s", path, newPath, err)
		}
		log.Printf(" => Changelog copied: %q\n", newPath)
		return nil
	}
}
