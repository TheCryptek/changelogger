package clog

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/bkc/changelogger/version"
)

const tagUrlFormat = ""

type Changelog struct {
	Version      map[string]*version.Version
	changelogDir string
	relDir       string
	unrelDir     string
}

func (c *Changelog) Populate(skipDir string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil || !info.IsDir() || strings.HasPrefix(path, skipDir) || path == c.changelogDir {
			return err
		}

		ver := strings.TrimPrefix(path, c.relDir)
		if len(ver) < 1 {
			return fmt.Errorf("invalid version path: %q: %q", path, ver)
		}

		log.Printf(" => seen %q in %q\n", ver, path)

		cl := &version.Version{}
		if err := filepath.Walk(c.changelogDir, cl.Populate(c.unrelDir)); err != nil {
			return fmt.Errorf("error populating changelog version: %q: %v\n", ver, err)
		}

		c.Version[ver] = cl

		return nil
	}
}

func New(changelogDir, relDir, unrelDir string) *Changelog {
	return &Changelog{
		Version:      make(map[string]*version.Version),
		changelogDir: changelogDir,
		relDir:       relDir,
		unrelDir:     unrelDir,
	}
}

func (c *Changelog) Write(w io.Writer) error {
	for ver, clv := range c.Version {
		if len(tagUrlFormat) == 0 {
			fmt.Fprintf(w, "## %s\n", ver)
		} else {
			fmt.Fprintf(w, "## [%s](%s)\n", ver, fmt.Sprintf(tagUrlFormat, ver))
		}
		clv.Write(w)
	}

	return nil
}
